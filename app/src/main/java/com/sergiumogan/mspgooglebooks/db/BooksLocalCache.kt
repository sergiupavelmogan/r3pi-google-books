package com.sergiumogan.mspgooglebooks.db

import android.util.Log
import androidx.paging.DataSource
import com.sergiumogan.mspgooglebooks.db.model.BookModel
import java.util.concurrent.Executor

class BooksLocalCache(
    private val booksDao: BooksDao,
    private val executor: Executor
) {

    fun insert(books: List<BookModel>, insertFinished: ()-> Unit) {
        executor.execute {
            Log.d("GoogleBooksCache", "inserting ${books.size} books")
            booksDao.insert(books)
            insertFinished()
        }
    }

    fun booksByName(name: String): DataSource.Factory<Int, BookModel> {
        val query = "%${name.replace(' ', '%')}%"
        return booksDao.booksByName(query)
    }

}