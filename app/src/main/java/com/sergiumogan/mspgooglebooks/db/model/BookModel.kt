package com.sergiumogan.mspgooglebooks.db.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "books")
data class BookModel (
    @PrimaryKey (autoGenerate = false) val id: String,
    val title: String? = "",
    val authors: String? = "",
    val categories: String? = "",
    val description: String? = "",
    val thumbnail: String? = ""
    )