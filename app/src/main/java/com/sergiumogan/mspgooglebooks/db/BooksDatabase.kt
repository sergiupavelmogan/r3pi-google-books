package com.sergiumogan.mspgooglebooks.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.sergiumogan.mspgooglebooks.db.model.BookModel

@Database(
    entities = [BookModel::class],
    version = 1,
    exportSchema = false
)
abstract class BooksDatabase: RoomDatabase() {

    abstract fun booksDao(): BooksDao

    companion object {

        @Volatile
        private var INSTANCE: BooksDatabase? = null

        fun getInstance(context: Context): BooksDatabase =
            INSTANCE ?: synchronized(this) {
                INSTANCE
                    ?: buildDatabase(context).also { INSTANCE = it }
            }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(context.applicationContext,
                BooksDatabase::class.java, "books.db")
                .build()
    }

}