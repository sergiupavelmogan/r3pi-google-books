package com.sergiumogan.mspgooglebooks.db

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.sergiumogan.mspgooglebooks.db.model.BookModel

@Dao
interface BooksDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(posts: List<BookModel>)

    @Query("SELECT * FROM books WHERE (title LIKE :query) OR (description LIKE :query) OR (authors LIKE :query) OR (categories LIKE :query)")
    fun booksByName(query: String): DataSource.Factory<Int, BookModel>

}