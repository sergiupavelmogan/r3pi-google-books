package com.sergiumogan.mspgooglebooks.model

import com.google.gson.annotations.SerializedName

data class PanelizationSummary (
    @field:SerializedName("containsEpubBubbles") val containsEpubBubbles: Boolean,
    @field:SerializedName("containsImageBubbles") val containsImageBubbles: Boolean
    )