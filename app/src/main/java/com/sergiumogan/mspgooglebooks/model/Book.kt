package com.sergiumogan.mspgooglebooks.model

import com.google.gson.annotations.SerializedName
import com.sergiumogan.mspgooglebooks.db.model.BookModel

class Book(
    @field:SerializedName("id") val id: String,
    @field:SerializedName("selfLink") val selfLink: String?,
    @field:SerializedName("volumeInfo") val volumeInfo: VolumeInfo?,
    @field:SerializedName("saleInfo") val saleInfo: SaleInfo?,
    @field:SerializedName("accessInfo") val accessInfo: AccessInfo?
) {

    fun toBookModel(): BookModel {
        return BookModel(id, volumeInfo?.title, volumeInfo?.getAuthorsString(), volumeInfo?.getCategoriesString(), volumeInfo?.description, volumeInfo?.imageLinks?.getThumbnailIfAvailable())
    }

}