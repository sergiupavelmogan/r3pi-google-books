package com.sergiumogan.mspgooglebooks.model

import com.google.gson.annotations.SerializedName

data class Epub (
    @field:SerializedName("isAvailable") val isAvailable: Boolean?
    )