package com.sergiumogan.mspgooglebooks.model

import com.google.gson.annotations.SerializedName
import com.sergiumogan.mspgooglebooks.R

class ImageLinks (
    @field:SerializedName("smallThumbnail") val smallThumbnail: String = "",
    @field:SerializedName("thumbnail") val thumbnail: String = "",
    @field:SerializedName("small") val small: String = "",
    @field:SerializedName("medium") val medium: String = "",
    @field:SerializedName("large") val large: String = ""
) {

    fun getMediumImageIfAvailable(): String? =
        when {
            medium.isNotEmpty() -> medium
            small.isNotEmpty() -> small
            thumbnail.isNotEmpty() -> thumbnail
            smallThumbnail.isNotEmpty() -> smallThumbnail
            else -> null
        }

    fun getThumbnailIfAvailable(): String? =
        when {
            smallThumbnail.isNotEmpty() -> smallThumbnail
            thumbnail.isNotEmpty() -> thumbnail
            small.isNotEmpty() -> small
            medium.isNotEmpty() -> medium
            else -> null
        }


}