package com.sergiumogan.mspgooglebooks.model

import com.google.gson.annotations.SerializedName
import java.util.*

data class VolumeInfo (
    @field:SerializedName("title") val title: String?,
    @field:SerializedName("authors") val authors: Array<String>?,
    @field:SerializedName("description") val description: String?,
    @field:SerializedName("publisher") val publisher: String?,
    @field:SerializedName("industryIdentifiers") val industryIdentifiers: Array<IndustryIdentifier>?,
    @field:SerializedName("readingModes") val readingModes: ReadingModes?,
    @field:SerializedName("pageCount") val pageCount: Int?,
    @field:SerializedName("printedPageCount") val printedPageCount: Int?,
    @field:SerializedName("printType") val printType: String?,
    @field:SerializedName("categories") val categories: Array<String>?,
    @field:SerializedName("maturityRating") val maturityRating: String?,
    @field:SerializedName("allowAnonLogging") val allowAnonLogging: Boolean?,
    @field:SerializedName("contentVersion") val contentVersion: String?,
    @field:SerializedName("panelizationSummary") val panelizationSummary: PanelizationSummary?,
    @field:SerializedName("imageLinks") val imageLinks: ImageLinks?,
    @field:SerializedName("language") val language: String?,
    @field:SerializedName("previewLink") val previewLink: String?,
    @field:SerializedName("infoLink") val infoLink: String?,
    @field:SerializedName("canonicalVolumeLink") val canonicalVolumeLink: String?
) {

    fun getAuthorsString(): String {
        val str = StringBuilder()
        if (authors != null && authors.isNotEmpty()) {
            var count = 0;
            for (s in authors) {
                count++
                str.append(s)
                if (count < authors.size) {
                    str.append(", ")
                }
            }
        }

        return str.toString()
    }

    fun getCategoriesString(): String {
        val str = StringBuilder()
        if (categories != null && categories.isNotEmpty()) {
            var count = 0;
            for (c in categories) {
                count++
                str.append(c)
                if (count < categories.size) {
                    str.append(", ")
                }
            }
        }

        return str.toString()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as VolumeInfo

        if (title != other.title) return false
        if (!Arrays.equals(authors, other.authors)) return false
        if (description != other.description) return false
        if (!Arrays.equals(industryIdentifiers, other.industryIdentifiers)) return false
        if (readingModes != other.readingModes) return false
        if (pageCount != other.pageCount) return false
        if (printedPageCount != other.printedPageCount) return false
        if (printType != other.printType) return false
        if (!Arrays.equals(categories, other.categories)) return false
        if (maturityRating != other.maturityRating) return false
        if (allowAnonLogging != other.allowAnonLogging) return false
        if (contentVersion != other.contentVersion) return false
        if (panelizationSummary != other.panelizationSummary) return false
        if (imageLinks != other.imageLinks) return false
        if (language != other.language) return false
        if (previewLink != other.previewLink) return false
        if (infoLink != other.infoLink) return false
        if (canonicalVolumeLink != other.canonicalVolumeLink) return false

        return true
    }

    override fun hashCode(): Int {
        var result = title?.hashCode() ?: 0
        result = 31 * result + (authors?.let { Arrays.hashCode(it) } ?: 0)
        result = 31 * result + (description?.hashCode() ?: 0)
        result = 31 * result + (industryIdentifiers?.let { Arrays.hashCode(it) } ?: 0)
        result = 31 * result + (readingModes?.hashCode() ?: 0)
        result = 31 * result + (pageCount ?: 0)
        result = 31 * result + (printedPageCount ?: 0)
        result = 31 * result + (printType?.hashCode() ?: 0)
        result = 31 * result + (categories?.let { Arrays.hashCode(it) } ?: 0)
        result = 31 * result + (maturityRating?.hashCode() ?: 0)
        result = 31 * result + (allowAnonLogging?.hashCode() ?: 0)
        result = 31 * result + (contentVersion?.hashCode() ?: 0)
        result = 31 * result + (panelizationSummary?.hashCode() ?: 0)
        result = 31 * result + (imageLinks?.hashCode() ?: 0)
        result = 31 * result + (language?.hashCode() ?: 0)
        result = 31 * result + (previewLink?.hashCode() ?: 0)
        result = 31 * result + (infoLink?.hashCode() ?: 0)
        result = 31 * result + (canonicalVolumeLink?.hashCode() ?: 0)
        return result
    }

}