package com.sergiumogan.mspgooglebooks.model

import com.google.gson.annotations.SerializedName

data class IndustryIdentifier (
    @field:SerializedName("type") val type: String,
    @field:SerializedName("identifier") val identifier: String
)