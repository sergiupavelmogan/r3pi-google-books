package com.sergiumogan.mspgooglebooks.model

import com.google.gson.annotations.SerializedName

data class Price (
    @field:SerializedName("amount") val amount: Float?,
    @field:SerializedName("amountInMicros") val amountInMicros: Long?,
    @field:SerializedName("currencyCode") val currencyCode: String?
    )