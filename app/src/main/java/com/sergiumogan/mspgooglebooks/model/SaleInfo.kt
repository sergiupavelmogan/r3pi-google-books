package com.sergiumogan.mspgooglebooks.model

import com.google.gson.annotations.SerializedName
import java.util.*

data class SaleInfo (
    @field:SerializedName("country") val country: String?,
    @field:SerializedName("saleability") val saleability: String?,
    @field:SerializedName("isEbook") val isEbook: Boolean?,
    @field:SerializedName("listPrice") val listPrice: Price?,
    @field:SerializedName("retailPrice") val retailPrice: Price?,
    @field:SerializedName("buyLink") val buyLink: String?,
    @field:SerializedName("offers") val offers: Array<Offer>?
    ) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as SaleInfo

        if (country != other.country) return false
        if (saleability != other.saleability) return false
        if (isEbook != other.isEbook) return false
        if (listPrice != other.listPrice) return false
        if (retailPrice != other.retailPrice) return false
        if (buyLink != other.buyLink) return false
        if (!Arrays.equals(offers, other.offers)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = country?.hashCode() ?: 0
        result = 31 * result + (saleability?.hashCode() ?: 0)
        result = 31 * result + (isEbook?.hashCode() ?: 0)
        result = 31 * result + (listPrice?.hashCode() ?: 0)
        result = 31 * result + (retailPrice?.hashCode() ?: 0)
        result = 31 * result + (buyLink?.hashCode() ?: 0)
        result = 31 * result + (offers?.let { Arrays.hashCode(it) } ?: 0)
        return result
    }
}