package com.sergiumogan.mspgooglebooks.model

import androidx.lifecycle.LiveData
import androidx.paging.PagedList
import com.sergiumogan.mspgooglebooks.db.model.BookModel

data class BooksSearchResult(
    val result: LiveData<PagedList<BookModel>>,
    val networkErrors: LiveData<String>
)