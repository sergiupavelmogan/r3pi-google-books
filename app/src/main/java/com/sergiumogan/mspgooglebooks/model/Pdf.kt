package com.sergiumogan.mspgooglebooks.model

import com.google.gson.annotations.SerializedName

data class Pdf (
    @field:SerializedName("isAvailable") val isAvailable: Boolean?,
    @field:SerializedName("acsTokenLink") val acsTokenLink: Boolean?
    )