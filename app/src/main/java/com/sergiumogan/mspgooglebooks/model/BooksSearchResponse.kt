package com.sergiumogan.mspgooglebooks.model

import com.google.gson.annotations.SerializedName
import com.sergiumogan.mspgooglebooks.db.model.BookModel

data class BooksSearchResponse(
    @SerializedName("totalItems") val totalItems: Int = 0,
    @SerializedName("items") val items: List<Book> = emptyList()
) {

    fun getDbModelItems(): List<BookModel> {
        val modelItems = ArrayList<BookModel>()

        for (book in items) {
            modelItems.add(book.toBookModel())
        }

        return modelItems
    }

}