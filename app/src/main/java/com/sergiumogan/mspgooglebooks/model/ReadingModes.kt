package com.sergiumogan.mspgooglebooks.model

import com.google.gson.annotations.SerializedName

data class ReadingModes (
    @field:SerializedName("text") val title: Boolean,
    @field:SerializedName("image") val image: Boolean
    )