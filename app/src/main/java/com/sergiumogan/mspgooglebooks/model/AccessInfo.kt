package com.sergiumogan.mspgooglebooks.model

import com.google.gson.annotations.SerializedName

data class AccessInfo (
    @field:SerializedName("country") val country: String?,
    @field:SerializedName("viewability") val viewability: String?,
    @field:SerializedName("embeddable") val embeddable: Boolean?,
    @field:SerializedName("publicDomain") val publicDomain: Boolean?,
    @field:SerializedName("textToSpeechPermission") val textToSpeechPermission: Boolean?,
    @field:SerializedName("epub") val epub: Epub?,
    @field:SerializedName("pdf") val pdf: Pdf?,
    @field:SerializedName("webReaderLink") val webReaderLink: String?,
    @field:SerializedName("accessViewStatus") val accessViewStatus: String?,
    @field:SerializedName("quoteSharingAllowed") val quoteSharingAllowed: Boolean?
    )