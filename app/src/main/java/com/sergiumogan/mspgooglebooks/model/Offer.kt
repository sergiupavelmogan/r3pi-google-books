package com.sergiumogan.mspgooglebooks.model

import com.google.gson.annotations.SerializedName

data class Offer (
    @field:SerializedName("finskyOfferType") val finskyOfferType: Int?,
    @field:SerializedName("listPrice") val listPrice: Price?,
    @field:SerializedName("retailPrice") val retailPrice: Price?
    )