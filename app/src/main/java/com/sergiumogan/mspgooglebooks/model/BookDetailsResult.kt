package com.sergiumogan.mspgooglebooks.model

import androidx.lifecycle.LiveData

data class BookDetailsResult (
    val result: LiveData<Book>,
    val networkErrors: LiveData<String>
)