package com.sergiumogan.mspgooglebooks.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.paging.PagedList
import com.sergiumogan.mspgooglebooks.data.BooksRepository
import com.sergiumogan.mspgooglebooks.db.model.BookModel
import com.sergiumogan.mspgooglebooks.model.BooksSearchResult

class MainViewModel(private val booksRepository: BooksRepository) : ViewModel() {

    private val query = MutableLiveData<String>()

    private val bookResult: LiveData<BooksSearchResult> = Transformations.map(query) {
        booksRepository.search(it)
    }

    val books: LiveData<PagedList<BookModel>> = Transformations.switchMap(bookResult) {
            it -> it.result
    }
    val networkErrors: LiveData<String> = Transformations.switchMap(bookResult) {
            it -> it.networkErrors
    }

    fun searchBooks(queryStr: String) {
        query.postValue(queryStr)
    }

}
