package com.sergiumogan.mspgooglebooks.ui.details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.sergiumogan.mspgooglebooks.data.BooksRepository
import com.sergiumogan.mspgooglebooks.model.Book
import com.sergiumogan.mspgooglebooks.model.BookDetailsResult

class BookDetailsViewModel(private val booksRepository: BooksRepository): ViewModel() {

    private val bookId = MutableLiveData<String>()

    private val bookDetailsResult: LiveData<BookDetailsResult> = Transformations.map(bookId) {
        booksRepository.getBookDetails(it)
    }

    val bookDetails: LiveData<Book> = Transformations.switchMap(bookDetailsResult) {
            it -> it.result
    }
    val networkErrors: LiveData<String> = Transformations.switchMap(bookDetailsResult) {
            it -> it.networkErrors
    }

    fun searchBooks(id: String) {
        bookId.postValue(id)
    }

}