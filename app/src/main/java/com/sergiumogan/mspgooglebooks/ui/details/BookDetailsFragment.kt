package com.sergiumogan.mspgooglebooks.ui.details

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.sergiumogan.mspgooglebooks.Injection
import com.sergiumogan.mspgooglebooks.R
import com.sergiumogan.mspgooglebooks.model.Book
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_book_details.*


class BookDetailsFragment : Fragment() {

    private lateinit var viewModel: BookDetailsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val detailArgs = BookDetailsFragmentArgs.fromBundle(arguments)
        val bookId = detailArgs.bookId

        viewModel = ViewModelProviders.of(this, Injection.provideViewModelFactory(requireContext()))
            .get(BookDetailsViewModel::class.java)
        viewModel.searchBooks(bookId)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_book_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.bookDetails.observe(this, Observer() {
            updateUi(it)
        })

        viewModel.networkErrors.observe(this, Observer {
            updateUi(it)
        })
    }

    private fun updateUi(book: Book) {
        Log.d("BOOK_DETAILS_ITEM", "Book id: ${book.volumeInfo?.title}")
        tvError.visibility = View.GONE
        progressBar.visibility = View.GONE
        container.visibility = View.VISIBLE
        book.volumeInfo?.imageLinks?.getMediumImageIfAvailable()?.let {
            Picasso.get()
                .load(it)
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .into(ivImage)
        }

        tvTitle.text = book.volumeInfo?.title
        tvAuthors.text = book.volumeInfo?.getAuthorsString()
        tvPages.text = book.volumeInfo?.pageCount?.toString()
        tvPublisher.text = book.volumeInfo?.publisher
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            tvDescription.text = Html.fromHtml(book.volumeInfo?.description?: "", Html.FROM_HTML_MODE_LEGACY)
        } else {
            tvDescription.text = Html.fromHtml(book.volumeInfo?.description?: "")
        }

        tvGooglePlayLink.setOnClickListener {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(book.volumeInfo?.infoLink)))
        }
        tvPreviewLink.setOnClickListener {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(book.volumeInfo?.previewLink)))
        }
    }

    private fun updateUi(error: String) {
        Log.d("BOOK_DETAILS_ITEM", "Error: $error")
        container.visibility = View.GONE
        progressBar.visibility = View.GONE
        tvError.text = error
        tvError.visibility = View.VISIBLE
    }

}
