package com.sergiumogan.mspgooglebooks.ui.viewholder

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView
import com.sergiumogan.mspgooglebooks.R
import com.sergiumogan.mspgooglebooks.db.model.BookModel
import com.squareup.picasso.Picasso

class BookViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    private val title: TextView = view.findViewById(R.id.tvTitle)
    private val authors: TextView = view.findViewById(R.id.tvAuthors)
    private val thumbnail: AppCompatImageView = view.findViewById(R.id.ivThumbnail)

    private var book: BookModel? = null

    fun bind(book: BookModel?) {
        if (book == null) {
            val resources = itemView.resources
            title.text = resources.getString(R.string.loading)
        } else {
            showBookData(book)
        }
    }

    private fun showBookData(book: BookModel) {
        this.book = book
        title.text = book.title
        authors.text = book.authors

        Picasso.get()
            .load(book.thumbnail)
            .placeholder(R.mipmap.ic_launcher)
            .error(R.mipmap.ic_launcher)
            .into(thumbnail)
    }

    companion object {
        fun create(parent: ViewGroup): BookViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.book_view_item, parent, false)
            return BookViewHolder(view)
        }
    }

}