package com.sergiumogan.mspgooglebooks.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.sergiumogan.mspgooglebooks.data.BooksRepository
import com.sergiumogan.mspgooglebooks.ui.details.BookDetailsViewModel
import com.sergiumogan.mspgooglebooks.ui.main.MainViewModel
import java.lang.IllegalArgumentException

class ViewModelFactory(): ViewModelProvider.NewInstanceFactory() {

    private lateinit var booksRepository: BooksRepository

    constructor(booksRepository: BooksRepository): this() {
        this.booksRepository = booksRepository
    }

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return MainViewModel(booksRepository) as T
        }
        if (modelClass.isAssignableFrom(BookDetailsViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return BookDetailsViewModel(booksRepository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel")
    }

}