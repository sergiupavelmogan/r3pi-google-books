package com.sergiumogan.mspgooglebooks.ui.main

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.paging.PagedList
import androidx.recyclerview.widget.DividerItemDecoration
import com.sergiumogan.mspgooglebooks.Injection
import com.sergiumogan.mspgooglebooks.R
import com.sergiumogan.mspgooglebooks.db.model.BookModel
import com.sergiumogan.mspgooglebooks.ui.adapter.BooksAdapter
import com.sergiumogan.mspgooglebooks.ui.details.BookDetailsFragment
import com.sergiumogan.mspgooglebooks.ui.details.BookDetailsFragmentArgs
import kotlinx.android.synthetic.main.main_fragment.*
import android.content.Context.INPUT_METHOD_SERVICE
import android.view.inputmethod.InputMethodManager
import androidx.core.content.ContextCompat.getSystemService



class MainFragment : Fragment(), BooksAdapter.OnClickListener {

    private lateinit var viewModel: MainViewModel
    private val booksAdapter = BooksAdapter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProviders.of(this, Injection.provideViewModelFactory(requireContext()))
            .get(MainViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val decoration = DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL)
        list.addItemDecoration(decoration)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupActions()
        setupObservers()

    }

    private fun setupActions() {
        etSearchBooks.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_GO) {
                triggerSearchBooks()
                true
            } else {
                false
            }
        }
        etSearchBooks.setOnKeyListener { _, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                triggerSearchBooks()
                true
            } else {
                false
            }
        }
    }

    private fun triggerSearchBooks() {
        etSearchBooks.text.trim().let {
            if (it.isNotEmpty()) {
                list.scrollToPosition(0)
                viewModel.searchBooks(it.toString())
                booksAdapter.submitList(null)
            }
        }
    }

    private fun setupObservers() {
        list.adapter = booksAdapter
        viewModel.books.observe(this, Observer<PagedList<BookModel>> {
            Log.d("Activity", "list: ${it?.size}")
            showEmptyList(it?.size == 0)
            booksAdapter.submitList(it)
            etSearchBooks.clearFocus()
            val `in` = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            `in`.hideSoftInputFromWindow(etSearchBooks.windowToken, 0)
        })
        viewModel.networkErrors.observe(this, Observer<String> {
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
        })
    }

    private fun showEmptyList(show: Boolean) {
        if (show) {
            emptyList.visibility = View.VISIBLE
            list.visibility = View.GONE
        } else {
            emptyList.visibility = View.GONE
            list.visibility = View.VISIBLE
        }
    }

    override fun onGoToBookDetails(id: String) {
        val direction =  MainFragmentDirections.actionGoToBookDetails().setBookId(id)
        this.view?.findNavController()?.navigate(direction)
    }

}
