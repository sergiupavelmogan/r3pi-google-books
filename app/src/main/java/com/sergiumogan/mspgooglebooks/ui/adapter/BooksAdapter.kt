package com.sergiumogan.mspgooglebooks.ui.adapter

import android.util.Log
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.sergiumogan.mspgooglebooks.db.model.BookModel
import com.sergiumogan.mspgooglebooks.ui.viewholder.BookViewHolder

class BooksAdapter(val clickListener: OnClickListener): PagedListAdapter<BookModel, RecyclerView.ViewHolder>(BOOK_COMPARATOR) {

    interface OnClickListener {
        fun onGoToBookDetails(id: String)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return BookViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val book = getItem(position)
        if (book != null) {
            (holder as BookViewHolder).bind(book)
        }
        holder.itemView.setOnClickListener{
            book?.id?.let { id ->
                clickListener.onGoToBookDetails(id)
            }
        }
    }

    companion object {
        private val BOOK_COMPARATOR = object : DiffUtil.ItemCallback<BookModel>() {
            override fun areItemsTheSame(oldItem: BookModel, newItem: BookModel): Boolean =
                oldItem.title == newItem.title

            override fun areContentsTheSame(oldItem: BookModel, newItem: BookModel): Boolean =
                oldItem == newItem
        }
    }

}