package com.sergiumogan.mspgooglebooks.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import com.sergiumogan.mspgooglebooks.R
import kotlinx.android.synthetic.main.main_activity.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        setSupportActionBar(toolbar)

        findNavController(R.id.navFragment).addOnNavigatedListener { controller, destination ->
            when (destination.id) {
                R.id.mainFragment -> {
                    supportActionBar?.setDisplayHomeAsUpEnabled(false)
                    supportActionBar?.title = getString(R.string.title_main)
                }
                R.id.bookDetailsFragment -> {
                    supportActionBar?.setDisplayHomeAsUpEnabled(true)
                    supportActionBar?.title = getString(R.string.title_details)
                }
            }
        }

    }

    override fun onSupportNavigateUp() =
        findNavController(R.id.navFragment).navigateUp()

}
