package com.sergiumogan.mspgooglebooks.api

import com.sergiumogan.mspgooglebooks.model.Book
import com.sergiumogan.mspgooglebooks.model.BooksSearchResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    @GET("volumes/")
    fun getVolumes(
        @Query("q") query: String,
        @Query("startIndex") startIndex: Int,
        @Query("maxResults") maxResults: Int
    ): Single<BooksSearchResponse>

    @GET("volumes/{volumeId}/")
    fun getVolumeDetails(
        @Path("volumeId") volumeId: String
    ): Single<Book>
}