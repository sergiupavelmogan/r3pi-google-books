package com.sergiumogan.mspgooglebooks

import android.content.Context
import androidx.lifecycle.ViewModelProvider
import com.sergiumogan.mspgooglebooks.api.ApiService
import com.sergiumogan.mspgooglebooks.api.RestClient
import com.sergiumogan.mspgooglebooks.db.BooksLocalCache
import com.sergiumogan.mspgooglebooks.data.BooksRepository
import com.sergiumogan.mspgooglebooks.db.BooksDatabase
import com.sergiumogan.mspgooglebooks.ui.ViewModelFactory
import java.util.concurrent.Executors

object Injection {

    /**
     * Creates an instance of [BooksLocalCache] based on the database DAO.
     */
    private fun provideCache(context: Context): BooksLocalCache {
        val database = BooksDatabase.getInstance(context)
        return BooksLocalCache(
            database.booksDao(),
            Executors.newSingleThreadExecutor()
        )
    }

    /**
     * Creates an instance of [BooksRepository] based on the [ApiService] and a
     * [BooksLocalCache]
     */
    private fun provideBooksRepository(context: Context): BooksRepository {
        return BooksRepository(RestClient.create(), provideCache(context))
    }

    /**
     * Provides the [ViewModelProvider.Factory] that is then used to get a reference to
     * [MainViewModel, BookDetailsViewModel] objects.
     */
    fun provideViewModelFactory(context: Context): ViewModelProvider.Factory {
        return ViewModelFactory(provideBooksRepository(context))
    }

}