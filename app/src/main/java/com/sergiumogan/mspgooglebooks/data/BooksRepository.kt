package com.sergiumogan.mspgooglebooks.data

import android.annotation.SuppressLint
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.LivePagedListBuilder
import com.sergiumogan.mspgooglebooks.api.ApiService
import com.sergiumogan.mspgooglebooks.db.BooksLocalCache
import com.sergiumogan.mspgooglebooks.model.Book
import com.sergiumogan.mspgooglebooks.model.BookDetailsResult
import com.sergiumogan.mspgooglebooks.model.BooksSearchResult
import io.reactivex.schedulers.Schedulers

class BooksRepository(
    private val apiService: ApiService,
    private val cache: BooksLocalCache
) {

    private val bookMutableLiveData = MutableLiveData<Book>()
    private val networkErrorsMutableLiveData = MutableLiveData<String>()

    fun search(query: String): BooksSearchResult {
        Log.d("GoogleBooks", "query: $query")

        val dataSourceFactory = cache.booksByName(query)

        val boundaryCallback = BooksBoundaryCallback(query, apiService, cache)
        val networkErrors = boundaryCallback.networkErrors

        val data = LivePagedListBuilder(dataSourceFactory, DATABASE_PAGE_SIZE)
            .setBoundaryCallback(boundaryCallback)
            .build()

        return BooksSearchResult(data, networkErrors)
    }

    @SuppressLint("CheckResult")
    fun getBookDetails(bookId: String): BookDetailsResult {
        apiService.getVolumeDetails(bookId)
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.single())
            .subscribe(
                { response ->
                    Log.d("BOOK_DETAILS", response.volumeInfo?.title)
                    bookMutableLiveData.postValue(response)
                },
                { error ->
                    Log.d("SERVER_RESPONSE", error.message)
                    networkErrorsMutableLiveData.postValue(error.message)
                }
            )
        return BookDetailsResult(bookMutableLiveData, networkErrorsMutableLiveData)
    }

    companion object {
        private const val DATABASE_PAGE_SIZE = 10
    }

}