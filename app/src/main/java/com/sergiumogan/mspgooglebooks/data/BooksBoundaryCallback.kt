package com.sergiumogan.mspgooglebooks.data

import android.annotation.SuppressLint
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.PagedList
import com.sergiumogan.mspgooglebooks.api.ApiService
import com.sergiumogan.mspgooglebooks.db.BooksLocalCache
import com.sergiumogan.mspgooglebooks.db.model.BookModel
import io.reactivex.schedulers.Schedulers

class BooksBoundaryCallback(
    private val query: String,
    private val service: ApiService,
    private val cache: BooksLocalCache
) : PagedList.BoundaryCallback<BookModel>() {

    companion object {
        private const val MAX_RESULTS = 20
    }

    private var startIndex = 0

    private val networkErrorsMutableLiveData = MutableLiveData<String>()

    val networkErrors: LiveData<String>
        get() = networkErrorsMutableLiveData

    private var isRequestInProgress = false

    @SuppressLint("CheckResult")
    private fun requestAndSaveData(query: String) {
        if (isRequestInProgress) return

        isRequestInProgress = true
        service.getVolumes(query, startIndex, MAX_RESULTS)
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.single())
            .subscribe(
                { response ->
                    Log.d("SERVER_RESPONSE", response.totalItems.toString())
                    cache.insert(response.getDbModelItems()) {
                        startIndex += MAX_RESULTS
                        isRequestInProgress = false
                    }
                },
                { error ->
                    Log.d("SERVER_RESPONSE", error.message)
                    networkErrorsMutableLiveData.postValue(error.message)
                    isRequestInProgress = false
                }
            )
    }

    override fun onZeroItemsLoaded() {
        requestAndSaveData(query)
    }

    override fun onItemAtEndLoaded(itemAtEnd: BookModel) {
        requestAndSaveData(query)
    }

}